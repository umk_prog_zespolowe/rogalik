﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    public bool isPaused;

    public Transform canvas;
    public Transform hud;

    private void Start()
    {
        isPaused = false;
    }

    // Update is called once per frame
    void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
            Pause();
        }

	}

    public void Pause()
    {
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            hud.gameObject.SetActive(false);
            isPaused = true;

            Time.timeScale = 0;
        }
        else
        {
            canvas.gameObject.SetActive(false);
            hud.gameObject.SetActive(true);
            isPaused = false;
            Time.timeScale = 1;
        }
    }


}