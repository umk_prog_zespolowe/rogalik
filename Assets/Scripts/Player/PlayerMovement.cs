﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed = 6f;    // The speed that the player will move at.
    float camRayLength = 100f;          // The length of the ray from the camera into the scene.
    int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    Vector3 movementInput;              // The vector to store the direction of the player's movement.

    // Use this for initialization
    void Start()
    {
        floorMask = LayerMask.GetMask("Floor");
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        // Store the input axes.
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        // Move the player around the scene.
        Move(h, v);

        // Turn the player to face the mouse cursor.
        Turning();
    }

    void Move(float h, float v)
    {
        // Set the movement vector based on the axis input.
        movementInput.Set(h, 0f, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        movementInput = movementInput.normalized * movementSpeed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        playerRigidbody.MovePosition(transform.position + movementInput);
    }

    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            playerRigidbody.MoveRotation(newRotation);   // transform.LookAt(new Vector3(playerToMouse.x,transform.position.y,playerToMouse.z));

            Debug.DrawLine(camRay.origin,playerToMouse, Color.blue);
        }
    }
}
