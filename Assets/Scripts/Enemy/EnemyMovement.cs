﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    // PlayerHealth playerHealth;
    // EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        // playerHealth = player.GetComponent<PlayerHealth>();
        // enemyHealth = GetComponent<EnemyHealth>();

        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }
    
	// Update is called once per frame
	void Update()
    {
        nav.SetDestination(player.position);
	}
}
