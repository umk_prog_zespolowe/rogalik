﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

    public float speed;
    public float lifetime;
    public int damageToGive;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        lifetime -= Time.deltaTime;
        if (lifetime <= 0)
            Destroy(gameObject);
	}

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag.Equals("Enemy"))
        {
            other.gameObject.GetComponent<EnemyHealth>().hurtEnemy(damageToGive);
            Destroy(gameObject);
        }
    }
}
